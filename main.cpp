
#include <cstdio>
#include <cmath>
#include "lib/initializer.h"
#include "pwm_controller.h"
#include "sensor.h"
#include "temperature_sensor.h"
#include "calculator.h"
#include "oled.h"

// Define target temperature delta above dew point
// (keep temperature TARGET_DELTA above dew point temperature)
#define TARGET_DELTA 1

// Define heating aggressiveness (less is more)
#define FULL_POWER_DELTA 10

#define PWM_WRAP 65535

using namespace std;

int main() {

    auto initializer = new Initializer(PWM_WRAP);

    auto oled = OLED();

    oled.setBrightness(1);

    auto sensor = Sensor(initializer->getI2CSensorChannel(), BME280_I2C_ADDR_PRIM);

    auto c1Heater = PwmController(C1_PWM, PWM_WRAP);
    auto c2Heater = PwmController(C2_PWM, PWM_WRAP);

    auto c1HeaterTemp = TemperatureSensor(PinOuts(C1_DATA));
    auto c2HeaterTemp = TemperatureSensor(PinOuts(C2_DATA));

    while (true) {

        auto readings = sensor.read();

        auto c1Temp = c1HeaterTemp.read();
        auto c2Temp = c2HeaterTemp.read();

        auto c1SensorAvailable = c1HeaterTemp.available();
        auto c2SensorAvailable = c2HeaterTemp.available();

        auto dewPointTemp = Calculator::dewPointTemperature(readings.temperature, readings.humidity);

        auto c1Delta = Calculator::temperatureDelta(dewPointTemp, c1Temp);
        auto c2Delta = Calculator::temperatureDelta(dewPointTemp, c2Temp);

        auto c1HeatingPower = c1SensorAvailable ? Calculator::heatingPower(c1Delta, TARGET_DELTA, FULL_POWER_DELTA) : 0;
        auto c2HeatingPower = c2SensorAvailable ? Calculator::heatingPower(c2Delta, TARGET_DELTA, FULL_POWER_DELTA) : 0;

//        printf("\e[2J");
//        printf("\n");
//        printf("-----------------------\n");
//        printf("Temperature: %.3f\n", readings.temperature);
//        printf("Relative Humidity: %.3f\n", readings.humidity);
//        printf("Dew point: %.3f\n", dewPointTemp);
//        printf("\n");
//        printf("[Channel1] current Temp: %.3f (dew point delta: %.3f)\n", c1Temp, c1Delta);
//        printf("[Channel2] current Temp: %.3f (dew point delta: %.3f)\n", c2Temp, c2Delta);
//        printf("\n");
//        printf("-----------------------\n");
//        printf("\n");
//        printf("-----------------------\n");
//        printf("[Channel1] HEATING set to: %d\n", c1HeatingPower);
//        printf("[Channel2] HEATING set to : %d\n", c2HeatingPower);
//        printf("-----------------------\n");

        c1Heater.setLevel(c1HeatingPower);
        c2Heater.setLevel(c2HeatingPower);

        oled.render({
                            readings.temperature,
                            readings.humidity,
                            readings.pressure,
                            c1Temp,
                            c2Temp,
                            c1HeatingPower,
                            c2HeatingPower,
                            dewPointTemp,
                            c1SensorAvailable,
                            c2SensorAvailable
                    });

    }
}

