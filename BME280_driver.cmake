add_library(sensor_bme280 INTERFACE)

set(LIBRARY_DIR vendor/BME280_driver)

target_include_directories(sensor_bme280 INTERFACE
        ${LIBRARY_DIR}
        )

target_sources(sensor_bme280 INTERFACE
        ${LIBRARY_DIR}/bme280.h ${LIBRARY_DIR}/bme280.c ${LIBRARY_DIR}/bme280_defs.h
        )

