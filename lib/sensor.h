//
// Created by Mariusz Henn on 07/05/2023.
//

#ifndef DEW_HEATER_CONTROLLER_SENSOR_H
#define DEW_HEATER_CONTROLLER_SENSOR_H

#include "bme280.h"
#include "hardware/i2c.h"

class Sensor {
public:
    Sensor(i2c_inst *i2cInst, uint8_t address);

    bme280_data read();

    void message();

private:
    bme280_dev *dev;
    uint32_t delay;

    void setup();

    void wakeUp();
};

#endif //DEW_HEATER_CONTROLLER_SENSOR_H
