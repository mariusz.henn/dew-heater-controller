//
// Created by Mariusz Henn on 08/05/2023.
//

#include "calculator.h"


double Calculator::dewPointTemperature(double temperature, double relativeHumidity) {
    return temperature - (100-relativeHumidity)/5;
}

double Calculator::temperatureDelta(double dewPointTemperature, double currentTemperature) {
    return currentTemperature - dewPointTemperature;
}

int Calculator::heatingPower(double temperatureDelta, int targetDelta, int fullPowerDelta) {

    // temp delta target for no heating must be
    double px = ((double )targetDelta - temperatureDelta)/(double )fullPowerDelta * 100;
    if (px > 100) {
        return 100;
    }
    if (px< 0) {
        return 0;
    }
    return (int)px;
}