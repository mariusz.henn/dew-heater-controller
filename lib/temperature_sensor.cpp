//
// Created by Mariusz Henn on 07/05/2023.
//

#include <cstdio>
#include <cmath>
#include "temperature_sensor.h"

TemperatureSensor::TemperatureSensor(PinOuts channel) : one_wire(channel) {
    connected = false;
}

bool TemperatureSensor::available() {
    return connected;
}

float TemperatureSensor::read() {
    if (!connected) {
        one_wire.init();
        one_wire.single_device_read_rom(address);
    }
    one_wire.convert_temperature(address, true, false);
    float currentTemp = one_wire.temperature(address);
    connected = currentTemp >= -999;
    return !connected ? NAN : currentTemp;
}

void TemperatureSensor::message() {
    float tmp = read();
    printf("Temperature: %3.1foC\n", tmp);


}
