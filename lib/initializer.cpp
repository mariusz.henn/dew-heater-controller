//
// Created by Mariusz Henn on 06/05/2023.
//
// GP2 - Channel 1  Heater PWM
// GP15 - Channel 2  Heater PWM
// GP16 - Channel 2 temperature sensor VCC
// GP17 - Channel 2 temperature Data (1wire) - pull up
// GP18 - Channel 1 temperature Data (1wire) - pull up
// GP19 - Channel 1 temperature sensor VCC
// GP26 - Temp/hum sensor I2C SDA
// GP27 - Temp/hum sensor I2C SCL
// GP28 - Temp/hum sensor VCC

#include <cstdio>
#include "initializer.h"
#include "pico/stdio.h"
#include "pinouts.h"
#include "hardware/gpio.h"
#include "hardware/pwm.h"
#include "hardware/i2c.h"
#include "DEV_Config.h"


Initializer::Initializer(int pwmWrapSetup) {
    this->pwmWrap = pwmWrapSetup;
    this->powerPeripherals();
    this->configurePWM();
    this->configureI2C();
    this->configure1Wire();
    stdio_init_all();
    this->configureOLED();
}

void Initializer::powerPeripherals() {
    PinOuts power[] = {OLED_RESET};
    for (PinOuts p: power) {
        gpio_init(p);
        gpio_set_dir(p, GPIO_OUT);
        gpio_put(p, true);
    }

}

void Initializer::configurePWM() {
    PinOuts pwm[] = {C1_PWM, C2_PWM, LED_PIN};
    for (PinOuts p: pwm) {
        gpio_set_function(p, GPIO_FUNC_PWM);
        gpio_set_dir(p, GPIO_OUT);
        uint slice = pwm_gpio_to_slice_num(p);
        pwm_set_clkdiv(slice, 1);
        uint channel = pwm_gpio_to_channel(p);
        pwm_set_wrap(slice, this->pwmWrap);
        pwm_set_chan_level(slice, channel, 0);
        pwm_set_enabled(slice, true);
    }
}

void Initializer::configureI2C() {
    i2c_inst *i2cChannel = getI2CSensorChannel();

    PinOuts i2c[] = {SENSOR_SCL, SENSOR_SDA, OLED_SCL, OLED_SDA};
    for (PinOuts p: i2c) {
        gpio_set_function(p, GPIO_FUNC_I2C);
        gpio_pull_up(p);
    }
    i2c_init(i2c0, 400000);
    i2c_init(i2cChannel, 19200);
}

void Initializer::configure1Wire() {
    PinOuts oneWire[] = {C1_DATA, C2_DATA};
    for (PinOuts p: oneWire) {
        gpio_init(p);
        gpio_pull_up(p);
        gpio_set_dir(p, GPIO_IN);
    }
}

void Initializer::configureOLED() {
    EPD_RST_PIN = OLED_RESET;
    EPD_SCL_PIN = OLED_SCL;
    EPD_SDA_PIN = OLED_SDA;
}

i2c_inst *Initializer::getI2CSensorChannel() {
    return i2c1;
}

bool reserved_addr(uint8_t addr) {
    return (addr & 0x78) == 0 || (addr & 0x78) == 0x78;
}

void Initializer::showI2cDevices() {

    i2c_inst *i2cChannel = getI2CSensorChannel();

    printf("\nI2C Bus Scan\n");
    printf("   0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F\n");

    for (int addr = 0; addr < (1 << 7); ++addr) {
        if (addr % 16 == 0) {
            printf("%02x ", addr);
        }

        int ret;
        uint8_t rxdata;
        if (reserved_addr(addr))
            ret = PICO_ERROR_GENERIC;
        else
            ret = i2c_read_blocking(i2cChannel, addr, &rxdata, 1, false);

        printf(ret < 0 ? "." : "@");
        printf(addr % 16 == 15 ? "\n" : "  ");
    }

}

