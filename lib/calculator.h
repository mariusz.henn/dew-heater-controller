//
// Created by Mariusz Henn on 08/05/2023.
//

#ifndef DEW_HEATER_CONTROLLER_CALCULATOR_H
#define DEW_HEATER_CONTROLLER_CALCULATOR_H


class Calculator {
public:
    static double dewPointTemperature(double temperature, double relativeHumidity);
    static double temperatureDelta(double dewPointTemperature, double currentTemperature);

    static int heatingPower(double temperatureDelta, int targetDelta, int fullPowerDelta);
};


#endif //DEW_HEATER_CONTROLLER_CALCULATOR_H
