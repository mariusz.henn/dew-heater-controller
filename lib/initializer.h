//
// Created by Mariusz Henn on 06/05/2023.
//

#ifndef DEW_HEATER_CONTROLLER_INITIALIZER_H
#define DEW_HEATER_CONTROLLER_INITIALIZER_H




#include "hardware/i2c.h"

class Initializer {
public:

    Initializer(int pwmWrapSetup);

     i2c_inst *getI2CSensorChannel();

     void showI2cDevices();

private:
     void powerPeripherals();

     void configurePWM();

     void configureI2C();

     void configure1Wire();

     void configureOLED();

     int pwmWrap;

};


#endif //DEW_HEATER_CONTROLLER_INITIALIZER_H
