//
// Created by Mariusz Henn on 07/05/2023.
//

#include "pwm_controller.h"
#include "hardware/pwm.h"
#include <cstdio>

void PwmController::setLevel(int level) {
    level = normalizeLevel(level);
    const uint16_t levelValue = this->wrap * level / 100;
    pwm_set_gpio_level(pin, levelValue);
    if (level == 0) {
        pwm_set_enabled(pin, false);
    } else {
        pwm_set_enabled(pin, true);
    }
    message(level);

}

PwmController::PwmController(PinOuts channel, int wrapInit) {
    pin = channel;
    wrap = wrapInit;
}

void PwmController::message(int level) const {
    printf("\nChannel: %i set to: %i\n", pin, level);
}

int PwmController::normalizeLevel(int level) {
    if (level > 100) {
        return 100;
    }
    if (level < 0) {
        return 0;
    }
    return level;
}

