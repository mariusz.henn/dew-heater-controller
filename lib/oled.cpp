//
// Created by Mariusz Henn on 11/05/2023.
//

#include <cstdlib>
#include "oled.h"
#include "DEV_Config.h"
#include "OLED_1in3_c.h"
#include "GUI_Paint.h"
#include "../assets/splash.h"
#include <string>
#include <cmath>
#include <locale>

inline std::string
to_string(double __val) {
    const int __n =
            __gnu_cxx::__numeric_traits<double>::__max_exponent10 + 20;
    return __gnu_cxx::__to_xstring<std::string>(&std::vsnprintf, __n,
                                                "%.2f", __val);
}

std::string label(const std::string &title, double value, const std::string &suffix = "") {
    std::string s = to_string(value);
    return title + ": " + s + suffix;
}


void OLED::setBrightness(int percent) {
    OLED_1in3_C_Brightness(percent);
}

void OLED::splashScreen() {
    Paint_Clear(BLACK);
    Paint_DrawBitMap(epd_bitmap_test);
    OLED_1in3_C_Display(BlackImage);
    OLED_1in3_C_On();
}

void OLED::render(OLEDData data) {
    Paint_Clear(BLACK);
    auto dewPoint = label("DEW", data.dewPoint, "C");
    auto temp = label("TMP", data.temp, "C");
    auto hum = label("HUM", data.hum, "%");
    auto c1t = data.c1SensorAvailable ? label(" T1", data.c1Temp, "C") : " T1: N/A";
    auto c2t = data.c2SensorAvailable  ? label(" T2", data.c2Temp, "C") : " T2: N/A";

    // stats
    Paint_DrawString_EN(0, 0, dewPoint.c_str(), &Font12, WHITE, BLACK);
    Paint_DrawString_EN(0, 13, temp.c_str(), &Font12, WHITE, BLACK);
    Paint_DrawString_EN(0, 26, hum.c_str(), &Font12, WHITE, BLACK);
    Paint_DrawString_EN(0, 39, c1t.c_str(), &Font12, WHITE, BLACK);
    Paint_DrawString_EN(0, 52, c2t.c_str(), &Font12, WHITE, BLACK);

    Paint_DrawLine(85, 0, 85, 64, WHITE, DOT_PIXEL_1X1, LINE_STYLE_SOLID);

    // graph

    Paint_DrawString_EN(86, 52, " C1 C2", &Font12, WHITE, BLACK);

    Paint_DrawRectangle(98, 1, 103, 50, WHITE, DOT_PIXEL_1X1, DRAW_FILL_EMPTY);
    Paint_DrawRectangle(119, 1, 124, 50, WHITE, DOT_PIXEL_1X1, DRAW_FILL_EMPTY);

    int height = 50;

    int c1Height = height - (int) floor(height * ((double) data.c1Power / 100));
    int c2Height = height - (int) floor(height * ((double) data.c2Power / 100));

    Paint_DrawRectangle(98, c1Height, 103, 50, WHITE, DOT_PIXEL_1X1, DRAW_FILL_FULL);
    Paint_DrawRectangle(119, c2Height, 124, 50, WHITE, DOT_PIXEL_1X1, DRAW_FILL_FULL);

    OLED_1in3_C_Display(BlackImage);
}


OLED::OLED() {
    OLED_1in3_C_Init();
    OLED_1in3_C_Clear();
    UWORD Imagesize =
            ((OLED_1in3_C_WIDTH % 8 == 0) ? (OLED_1in3_C_WIDTH / 8) : (OLED_1in3_C_WIDTH / 8 + 1)) * OLED_1in3_C_HEIGHT;
    if ((BlackImage = (UBYTE *) malloc(Imagesize)) == NULL) {
        while (1) {
            printf("Failed to apply for black memory...\r\n");
        }
    }
    Paint_NewImage(BlackImage, OLED_1in3_C_WIDTH, OLED_1in3_C_HEIGHT, 0, WHITE);
    Paint_SelectImage(BlackImage);
    splashScreen();
}