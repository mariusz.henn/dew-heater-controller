//
// Created by Mariusz Henn on 07/05/2023.
//

#ifndef DEW_HEATER_CONTROLLER_TEMPERATURE_SENSOR_H
#define DEW_HEATER_CONTROLLER_TEMPERATURE_SENSOR_H


#include "pinouts.h"
#include "one_wire.h"

class TemperatureSensor {
public:
    explicit TemperatureSensor(PinOuts channel);
    float read();
    void message();
    bool available();
private:
    PinOuts pin;
    rom_address_t address{};
    One_wire one_wire;
    bool connected;

};


#endif //DEW_HEATER_CONTROLLER_TEMPERATURE_SENSOR_H
