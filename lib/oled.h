//
// Created by Mariusz Henn on 11/05/2023.
//

#ifndef DEW_HEATER_CONTROLLER_OLED_H
#define DEW_HEATER_CONTROLLER_OLED_H


#include <cstdint>


struct OLEDData {
    double temp;
    double hum;
    double press;
    double c1Temp;
    double c2Temp;
    int c1Power;
    int c2Power;
    double dewPoint;
    bool c1SensorAvailable;
    bool c2SensorAvailable;
};

class OLED {
private:
    uint8_t *BlackImage;

    void splashScreen();

public:
    OLED();

    void render(OLEDData data);

    void setBrightness(int percent);


};


#endif //DEW_HEATER_CONTROLLER_OLED_H
