//
// Created by Mariusz Henn on 07/05/2023.
//

#ifndef DEW_HEATER_CONTROLLER_PWM_CONTROLLER_H
#define DEW_HEATER_CONTROLLER_PWM_CONTROLLER_H


#include "pinouts.h"

class PwmController {
private:
    PinOuts pin;
    int wrap;

    void message(int level) const;

    static int normalizeLevel(int level);

public:
    explicit PwmController(PinOuts channel, int wrapInit);

    void setLevel(int level0);
};


#endif //DEW_HEATER_CONTROLLER_PWM_CONTROLLER_H
