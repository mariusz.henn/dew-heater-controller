//
// Created by Mariusz Henn on 07/05/2023.
//

#include <cstdio>
#include <cstring>
#include "bme280.h"
#include "sensor.h"
#include "hardware/i2c.h"

struct Config {
    i2c_inst *i2c;
    uint8_t address;
};

void delay_us(uint32_t period, void *intf_ptr) {
    sleep_ms(period);
}



BME280_INTF_RET_TYPE bme280_i2c_read(uint8_t reg_addr, uint8_t *reg_data, uint32_t len, void *intf_ptr) {
    auto cfg = *(Config *) intf_ptr;
    int err = i2c_write_blocking(cfg.i2c, cfg.address, &reg_addr, 1, true);
    if (err == PICO_ERROR_GENERIC) {
        return BME280_E_COMM_FAIL;
    }
    err = i2c_read_blocking(cfg.i2c, cfg.address, reg_data, len, false);
    if (err == PICO_ERROR_GENERIC) {
        return BME280_E_COMM_FAIL;
    }
    return BME280_INTF_RET_SUCCESS;
}

BME280_INTF_RET_TYPE bme280_i2c_write(uint8_t reg_addr, const uint8_t *reg_data, uint32_t length, void *intf_ptr) {
    auto cfg = *(Config *) intf_ptr;
    uint8_t tx_len = length + 1;
    uint8_t tx_buf[tx_len];
    tx_buf[0] = reg_addr;
    memcpy(&tx_buf[1], reg_data, length);
    int err = i2c_write_blocking(cfg.i2c, cfg.address, tx_buf, tx_len, false);
    if (err == PICO_ERROR_GENERIC) {
        return BME280_E_COMM_FAIL;
    }
    return BME280_INTF_RET_SUCCESS;
}

Sensor::Sensor(i2c_inst *i2cInst, uint8_t address) {

    auto *cfg = new Config();

    cfg->address= address;
    cfg->i2c = i2cInst;

    dev = new bme280_dev;
    dev->chip_id = cfg->address;
    dev->read = bme280_i2c_read;
    dev->write = bme280_i2c_write;
    dev->intf = BME280_I2C_INTF;
    dev->intf_ptr = cfg;
    dev->delay_us = delay_us;

    int8_t err = bme280_init(dev);
    if (err != 0) {
        printf("[ERROR] Init BME280 failed with: %i\n", err);
    }

    setup();

}

void Sensor::setup() {
    struct bme280_settings settings = {0};

    uint8_t error = bme280_get_sensor_settings(&settings, dev);
    if (error != BME280_OK) {
        fprintf(stderr, "[ERROR] BME280 Settings reading failed with: %+d\n", error);
        return;
    }

    settings.filter = BME280_FILTER_COEFF_OFF;
    settings.osr_h = BME280_OVERSAMPLING_1X;
    settings.osr_p = BME280_OVERSAMPLING_1X;
    settings.osr_t = BME280_OVERSAMPLING_1X;

    error = bme280_set_sensor_settings(BME280_SEL_ALL_SETTINGS, &settings, dev);
    if (error != BME280_OK) {
        fprintf(stderr, "[ERROR] BME280 Settings set failed with: %+d\n", error);
        return;
    }
    bme280_cal_meas_delay(&delay, &settings);
}

void Sensor::wakeUp() {
    int8_t err = bme280_set_sensor_mode(BME280_POWERMODE_FORCED, dev);
    if (err != 0) {
        printf("[ERROR] Set BME280 mode failed with: %i\n", err);
    }
}

bme280_data Sensor::read() {

    bme280_data comp_data{};

    wakeUp();

    //dev->delay_us(delay, dev->intf_ptr);
    uint8_t err = bme280_get_sensor_data(BME280_ALL, &comp_data, dev);
    if (err != BME280_OK) {
        printf("[ERROR] BME280 read error with: %i\n", err);
    }
    return comp_data;
}

void Sensor::message() {
    bme280_data reading = read();
    printf("\n------SENSOR READINGS------\n");
    printf("Temperature: %.2f°C, Pressure: %.2f Pa, Humidity: %.2f%%\n",
           reading.temperature, reading.pressure, reading.humidity);
    printf("---------------------------\n");

}