//
// Created by Mariusz Henn on 06/05/2023.
//

#ifndef DEW_HEATER_CONTROLLER_PINOUTS_H
#define DEW_HEATER_CONTROLLER_PINOUTS_H

enum PinOuts {
    LED_PIN = 25,
    C1_PWM = 2, // PWM 1 B
    C2_PWM = 15,// PWM 7 B
    C2_DATA = 17,
    C1_DATA = 18,
    SENSOR_SDA = 26, // I2C1
    SENSOR_SCL = 27, // I2C1
    OLED_RESET = 11,
    OLED_SDA = 12, // I2C0
    OLED_SCL = 13, // I2C0
};

#endif //DEW_HEATER_CONTROLLER_PINOUTS_H
